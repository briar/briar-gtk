# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
# Translators:
# erinm, 2020
# Cristina Lupu <cristina@cji.ro>, 2020
# titus <titus0818@gmail.com>, 2020
# Vitalie Ciubotaru <vitalie@ciubotaru.tk>, 2020
# Licaon_Kter, 2021
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"PO-Revision-Date: 2020-09-18 11:09+0000\n"
"Last-Translator: Licaon_Kter, 2021\n"
"Language-Team: Romanian (https://www.transifex.com/otf/teams/76528/ro/)\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1?0:(((n%100>19)||((n%100==0)&&(n!=0)))?2:1));\n"

msgctxt "Used in add contact page"
msgid "Give this link to the contact you want to add"
msgstr "Dați această adresă contactului pe care doriți să-l adăugați"

msgctxt "Used in add contact page"
msgid "Enter the link from your contact here"
msgstr "Introduceți adresa contactului dumneavoastră aici"

msgctxt "Used in add contact page"
msgid "Contact's link"
msgstr "Adresa contactului"

msgctxt "Used in add contact page"
msgid "Please enter a link"
msgstr "Vă rugăm să introduceți o adresă"

msgctxt "Used in add contact page"
msgid "Give your contact a nickname. Only you can see it."
msgstr "Introduceți un nume pentru contact. Vizibil doar pentru dumneavoastră."

msgctxt "Used in add contact page"
msgid "Enter a nickname"
msgstr "Introduceți un nume"

msgctxt "Used in add contact page"
msgid "Please enter a nickname"
msgstr "Vă rugăm să introduceți un nume"

msgctxt "Button in add contact page"
msgid "Add contact"
msgstr "Adaugă contact"

msgid "Next"
msgstr "Înainte"

msgctxt "Title in add contact page"
msgid "Add contact"
msgstr "Adaugă contact"

msgid "Change contact name"
msgstr "Schimbă nume contact"

msgid "Delete all messages"
msgstr "Șterge toate mesajele"

msgid "Delete contact"
msgstr "Șterge contactul"

msgctxt "Accessibility text in chat input field"
msgid "Show emoji menu"
msgstr "Arată meniul cu emoticoane"

msgid "No contact selected"
msgstr "Nici un contact selectat"

msgid "Select a contact to start chatting"
msgstr "Selectați un contact pentru a începe o conversație"

msgctxt "Accessibility text in main menu"
msgid "Add contact"
msgstr "Adaugă contact"

msgid "Main menu"
msgstr "Meniu principal"

msgid "Back"
msgstr "Înapoi"

msgid "Chat menu"
msgstr "Meniu conversație"

msgid "Password"
msgstr "Parola"

msgctxt "Button in login page"
msgid "Log in"
msgstr "Autentificare"

msgctxt "Used in login page after entering the password"
msgid "Logging in"
msgstr "Autentificare"

msgid "About Briar GTK"
msgstr "Despre Briar GTK"

msgid "Welcome to Briar"
msgstr "Bine ați venit la Briar"

msgid "Choose your nickname"
msgstr "Alegeți-vă numele de utilizator"

msgid "Confirm Password"
msgstr "Confirmare parolă"

msgid "Create Account"
msgstr "Creează un cont"

msgid "Creating Account"
msgstr "Crearea contului"

msgid "Please enter a nickname"
msgstr "Vă rugăm să introduceți un nume"

msgid "Please enter a password"
msgstr "Vă rugăm să introduceți o parolă"

msgid "The passwords do not match"
msgstr "Parolele nu se potrivesc"

msgid "Couldn't register account"
msgstr "Nu s-a putut înregistra un cont"

msgid "Couldn't log in"
msgstr "Nu s-a putut face autentificarea"

msgid "Localization Lab Translation Teams"
msgstr "Echipa de traduceri Localization Lab"

msgid "Using code by"
msgstr "Folosind cod"

msgid "Briar functionality by"
msgstr "Funcționalitate Briar"

msgid "Please enter a link"
msgstr "Vă rugăm să introduceți o adresă"

msgid "Enter your contact's link, not your own"
msgstr "Introduceți adresa contactului, nu cea personală"

msgid "Invalid link"
msgstr "Adresă invalidă"

msgid "There was an error adding the contact"
msgstr "A apărut o eroare la adăugarea contactului"

#, python-format
msgid "Contact %s already exists"
msgstr "Contactul %s există deja"

msgid "Pending contact updated"
msgstr "Listă solicitări de contact în așteptare actualizată"

msgid "An error occurred while updating the pending contact"
msgstr "A apărut o eroare în timpul actualizării cererii contactului"

msgid "Duplicate Link"
msgstr "Adresă duplicat"

#, python-format
msgid "You already have a pending contact with this link: %s"
msgstr "Deja aveți un contact în așteptare cu această adresă: %s"

#, python-format
msgid "Are %s and %s the same person?"
msgstr "Sunt %s  și %s aceeași persoană?"

#, python-format
msgid ""
"%s and %s sent you the same link.\n"
"\n"
"One of them may be trying to discover who your contacts are.\n"
"\n"
"Don't tell them you received the same link from someone else."
msgstr ""
"%s și %s v-au trimis aceeași adresă.\n"
"\n"
"Unii dintre ei s-ar putea să încerce să descopere care vă sunt contactele.\n"
"\n"
"Nu le spuneți că ați primit aceeași adresă și de la altcineva."

msgid "Contact added"
msgstr "Contact adăugat"

msgid "New private message"
msgstr "Mesaj privat nou"

msgid "Are you sure you want to exit?"
msgstr "Sunteți siguri că doriți să ieșiți?"

msgid "Once you close Briar, you'll no longer receive messages nor send pending ones. Keep Briar open to stay connected with your contacts."
msgstr "Odată Briar închis, nu veți mai primi mesaje și nu veți trimite mesajele în curs. Păstrați Briar deschis pentru a sta conectați la contactele dumneavoastră."

msgid "Change"
msgstr "Modifică"

msgid "Confirm Message Deletion"
msgstr "Confirmare ștergere mesaj"

msgid "Are you sure that you want to delete all messages?"
msgstr "Sigur doriți să ștergeți toate mesajele?"

msgid "Confirm Contact Deletion"
msgstr "Confirmare ștergere contact"

msgid "Are you sure that you want to remove this contact and all messages exchanged with this contact?"
msgstr "Sigur doriți să ștergeți acest contact și toate mesajele schimbate?"

msgid "Myself"
msgstr "Eu"

msgctxt "Used in about dialog"
msgid "Copyright © 2019-2021 The Briar Project"
msgstr "Drepturi de autor © 2019-2021 Proiectul Briar"

msgctxt "Used in about dialog"
msgid "A Briar client for GTK desktop and mobile devices"
msgstr "Un client Briar pentru calculatoare și dispozitive mobile cu GTK"

msgctxt "Used in about dialog to get to website"
msgid "Visit Briar GTK website"
msgstr "Vizitare site Briar GTK"
